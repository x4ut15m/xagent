package com.xagent.core;

/**
 * Author: 4ut15m
 * 线程控制，确保不会重复进入Agent
 * */
public interface ThreadControl {
    boolean isEnter();
    void setEnter(boolean enter);
    Class<?> getEntryBClass();
    void setEntryBClass(Class<?> entryB);

}
