package com.xagent.core;

import java.lang.instrument.Instrumentation;

public interface Entry {

    void entrance(Instrumentation instrumentation,String filePath);

    void entrance(Object returnObject, Object thisObject, Object[] args, boolean isEnter, int index, String className, String methodName, String methodDescirptor);

}
