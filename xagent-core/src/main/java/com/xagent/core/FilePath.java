package com.xagent.core;


import java.io.File;

/**
 * Author: 4ut15m
 * 存放agent相关jar包的信息
 */
public class FilePath {
    private String baseFilePath;

    public FilePath(String baseFilePath) {
        this.baseFilePath = baseFilePath;
    }

    public String getBaseFilePath() {
        return baseFilePath;
    }

    public String getCorePath() {
        return this.baseFilePath + File.separator + "xagent-core.jar";
    }

    public String getEnginePath() {
        return this.baseFilePath + File.separator + "xagent-engine.jar";
    }

}
