package com.xagent.core;

public class Properties {
    /**
     * Agent 可动态修改的配置 start
     * */

    /**
     * 作用和XAGENT_AGENT_PATH差不多，但是用于engine包，并且该值动态可变
     */
    public static String XAGENT_BASE_PATH = "";

    /**
     * Agent 可动态修改的配置 end
     * */

    /**
     * jvm properties start.
     * Start with XAGENT_
     */
    // 指定agent、core、engine所在的目录
    public static final String XAGENT_AGENT_PATH = "xagent.agent.path";

    // 策略文件的路径
    public static final String XAGENT_POLICY_PATH = "xagent.policy.path";
    // 指定日志文件保存的路径
    public static final String XAGENT_LOG_PATH = "xagent.log.path";
    // 控制调试，仅值为true时开启调试
    public static final String XAGENT_DEBUG_MODE = "xagent.debug.mode";

    /**
     * jvm properties end.
     * */

    /**
     * common properties start.
     * Start with COMMON_
     */
    // engine入口类
    public static final String COMMON_ENGINE_ENTRY_CLASS = "com.xagent.engine.ReflectEntry";
    // 589824 为ASM9.0
    public static final int COMMON_ASM_VERSION = 589824;
    public static final String COMMON_LOGGER_AGENT = "com.xagent.logger.agentlogger";
    public static final String COMMON_LOGGER_SECURITY = "com.xagent.logger.securitylogger";
    /**
     * common properties end.
     */


}
