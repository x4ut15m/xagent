package com.xagent.core;

public class ThreadControlEnter implements ThreadControl {

    private Class entryBClass;
    /**
     * 是否已经进入Agent
     */
    private boolean enter;

    @Override
    public boolean isEnter() {
        return enter;
    }

    @Override
    public void setEnter(boolean enter) {
        this.enter = enter;
    }

    @Override
    public Class<?> getEntryBClass() {
        return entryBClass;
    }

    @Override
    public void setEntryBClass(Class<?> entryB) {
        this.entryBClass = entryB;
    }

}
