package com.xagent.agent.agent;

import com.xagent.agent.Launcher;

import java.lang.instrument.Instrumentation;
/**
 * Author: 4ut15m
 * Agent起点
 * */
public class Agent {
    private static final String BANNER = " __  __    ___     ___     ___    _  _    _____  \n" +
            " \\ \\/ /   /   \\   / __|   | __|  | \\| |  |_   _| \n" +
            "  >  <    | - |  | (_ |   | _|   | .` |    | |   \n" +
            " /_/\\_\\   |_|_|   \\___|   |___|  |_|\\_|   _|_|_  \n" +
            "_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"| \n" +
            "\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-' ";
    private static final String AUTHOR = " - Author: 4ut15m\n";

    public static void premain(String args, Instrumentation instrumentation) throws Exception {
        System.out.println(BANNER+AUTHOR);
        System.out.println("[XAGENT] xagent was running.");
        Launcher launcher = new Launcher(instrumentation, args);
        launcher.start();
    }

    public static void agentmain(String args, Instrumentation instrumentation) throws Exception{
        System.out.println(BANNER+AUTHOR);
        Launcher launcher = new Launcher(instrumentation, args);
        launcher.start();
    }

    public static void init(String args, Instrumentation instrumentation) {
        /**
         * 解析-javaagent传入的和通过-D设置的参数
         * */

    }


}
