package com.xagent.engine.policy;

import com.xagent.engine.enums.Phase;
import com.xagent.engine.hook.MethodInfo;

/**
 * Author: 4ut15m
 * 记录、保存方法上下文
 */
public class MethodContext {
    // Hook点的类型id，比如RR001
    private String id;
    /**
     * Hook点对应的阶段
     * SOURCE 污染原点
     * PROPAGATE 传播点
     * SINK 触发点
     */
    private Phase phase;
    // 当前方法的类名
    private String className;
    // 方法名
    private String methodName;
    // 方法描述符
    private String methodDescriptor;
    // 返回值的类型
    private String returnClass;

    // 当前方法对象，静态方法，该值为null
    private Object thisObject;
    // 当前方法的返回对象
    private Object returnObject;
    // 当前方法的参数，没有参数时为null
    private Object[] paramObjects;
    private boolean isEnter;
    // 索引
    private int index;
    // 存放的hook信息
    private MethodInfo methodInfo;

    public MethodContext(String className, String methodName, String methodDescriptor) {
        this.className = className;
        this.methodName = methodName;
        this.methodDescriptor = methodDescriptor;
    }

    /**
     * getter start.
     */
    public String getClassName() {
        return className;
    }

    public String getMethodDescriptor() {
        return methodDescriptor;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getId() {
        return id;
    }

    public String getReturnClass() {
        return returnClass;
    }

    public Phase getPhase() {
        return phase;
    }

    public Object getReturnObject() {
        return returnObject;
    }

    public Object getThisObject() {
        return thisObject;
    }

    public Object[] getParamObjects() {
        return paramObjects;
    }

    public boolean isEnter() {
        return isEnter;
    }

    public int getIndex() {
        return index;
    }

    public String getSignature() {
        return methodName + methodDescriptor;
    }

    public MethodInfo getMethodInfo() {
        return methodInfo;
    }

    /**
     * getter end.
     * <p>
     * setter start.
     */

    public void setEnter(boolean enter) {
        isEnter = enter;
    }

    public void setReturnObject(Object returnObject) {
        this.returnObject = returnObject;
    }

    public void setThisObject(Object thisObject) {
        this.thisObject = thisObject;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }

    public void setParamObjects(Object[] paramObjects) {
        this.paramObjects = paramObjects;
    }


    public void setIndex(int index) {
        this.index = index;
    }


    public void setId(String id) {
        this.id = id;
    }

    public void setMethodInfo(MethodInfo methodInfo) {
        this.methodInfo = methodInfo;
    }
    /**
     * setter end.
     * */
}
