package com.xagent.engine;

import com.xagent.core.Entry;
import com.xagent.core.FilePath;
import com.xagent.engine.config.Config;
import com.xagent.engine.config.ConfigImpl;
import com.xagent.engine.context.AgentContext;
import com.xagent.engine.context.inter.Context;
import com.xagent.engine.plugins.HookPlugin;
import com.xagent.engine.plugins.Plugin;
import com.xagent.engine.plugins.StartupPlugin;

import java.lang.instrument.Instrumentation;

public class ReflectEntry implements Entry {

    @Override
    public void entrance(Instrumentation instrumentation, String filePath) {
        Config config = new ConfigImpl(new FilePath(filePath));
        AgentContext.createContext(config, instrumentation);
        Context context = AgentContext.getContext();
        context.setPlugins(new Plugin[]{
                new StartupPlugin(),
                new HookPlugin()
        });

        for (Plugin plugin : context.getPlugins()) {
            context.getAgentLogger().info("[Agent] " + "Running plugin: " + plugin.getPluginName());
            try {
                plugin.start(context);
            } catch (Exception e) {
                context.getAgentLogger().error("[Agent] " + plugin.getPluginName() + " start failed.error information: " + e.getMessage());
            }
        }

    }

    @Override
    public void entrance(Object returnObject, Object thisObject, Object[] args, boolean isEnter, int index, String className, String methodName, String methodDescirptor) {

    }


}
