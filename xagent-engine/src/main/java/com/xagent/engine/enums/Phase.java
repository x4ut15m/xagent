package com.xagent.engine.enums;


public enum Phase {
    SOURCE,
    PROPAGATE,
    SINK
}
