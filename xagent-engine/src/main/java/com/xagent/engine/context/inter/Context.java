package com.xagent.engine.context.inter;

import com.xagent.engine.config.Config;
import com.xagent.engine.plugins.Plugin;
import org.slf4j.Logger;

public interface Context {

    Config getConfig();

    InstWrapper getInstWrapper();

    void setPlugins(Plugin[] plugins);

    Plugin[] getPlugins();

    Logger getAgentLogger();
    Logger getSecurityLogger();

    boolean isDebug();

}
