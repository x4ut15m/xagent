package com.xagent.engine.context.inter;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;

public interface InstWrapper {

    void addTransformer(ClassFileTransformer transformer);

    void retransformClasses(Class<?> clazz) throws UnmodifiableClassException;

    void retransformAllClasses() throws UnmodifiableClassException;

    Instrumentation getInstrumentation();

    void removeTransformer(ClassFileTransformer transformer);

    boolean addClassLoader(ClassLoader classLoader);
    boolean filterOrEnhance(String className);
}
