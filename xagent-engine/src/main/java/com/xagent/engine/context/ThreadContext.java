package com.xagent.engine.context;

import com.xagent.core.ThreadControl;
import com.xagent.engine.context.inter.ITaintPool;
import com.xagent.engine.context.phase.AbstractPhase;

public class ThreadContext implements ThreadControl {
    private AbstractPhase phase;
    private Object request;
    private boolean enter;
    private ITaintPool taintPool;

    public AbstractPhase getPhase() {
        return phase;
    }

    public Object getRequest() {
        return request;
    }

    @Override
    public boolean isEnter() {
        return enter;
    }

    @Override
    public Class<?> getEntryBClass() {
        return null;
    }

    public ITaintPool getTaintPool() {
        return taintPool;
    }

    @Override
    public void setEnter(boolean enter) {
        this.enter = enter;
    }

    @Override
    public void setEntryBClass(Class<?> entryB) {
    }

    public void setRequest(Object request) {
        this.request = request;
    }

    public void setPhase(AbstractPhase phase) {
        this.phase = phase;
    }

    public void setTaintPool(ITaintPool taintPool) {
        this.taintPool = taintPool;
    }

}
