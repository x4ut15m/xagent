package com.xagent.engine.context.phase;

import com.xagent.engine.enums.Phase;
import com.xagent.engine.policy.MethodContext;

public abstract class AbstractPhase {
    protected AbstractPhase parentPhase;
    protected MethodContext methodContext;
    protected Phase phaseStatus;


    public MethodContext getMethodContext() {
        return methodContext;
    }


    public void setParentPhase(AbstractPhase parentPhase) {
        this.parentPhase = parentPhase;
    }

    public Phase getPhaseStatus() {
        return phaseStatus;
    }

    public void setPhaseStatus(Phase phaseStatus) {
        this.phaseStatus = phaseStatus;
    }
}
