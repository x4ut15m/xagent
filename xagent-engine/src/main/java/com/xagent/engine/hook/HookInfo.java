package com.xagent.engine.hook;

import java.util.HashMap;

public interface HookInfo {

     String getClassName();

     HashMap<String, MethodInfo> getMethodInfoHashMap();

     void addMethodInfo(MethodInfo methodInfo);
     boolean needEnhance(String method);

     boolean isHook(String method);
     MethodInfo getAMethodInfo(String methodWithoutReturnType);
}
