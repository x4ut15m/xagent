package com.xagent.engine.hook.impl;


import com.xagent.engine.hook.HookFile;

public class HookFileImpl implements HookFile {
    private String sourceFilePath;
    private String propagateFilePath;
    private String sinkFilePath;

    private String sourceFileContent;
    private String propagateFileContent;
    private String sinkFileContent;


    public HookFileImpl(String sf, String pf, String skf) {
        sourceFileContent = sf;
        propagateFileContent = pf;
        sinkFileContent = skf;
    }

    public HookFileImpl(String sf, String pf, String skf, String sfc, String pfc, String skfc) {
        sourceFilePath = sf;
        propagateFilePath = pf;
        sinkFilePath = skf;
        sourceFileContent = sfc;
        propagateFileContent = pfc;
        sinkFileContent = skfc;
    }

    @Override
    public String getSourceFilePath() {
        return sourceFilePath;
    }

    @Override
    public String getPropagateFilePath() {
        return propagateFilePath;
    }

    @Override
    public String getSinkFilePath() {
        return sinkFilePath;
    }

    public String getPropagateFileContent() {
        return propagateFileContent;
    }

    public String getSinkFileContent() {
        return sinkFileContent;
    }

    public String getSourceFileContent() {
        return sourceFileContent;
    }
}
