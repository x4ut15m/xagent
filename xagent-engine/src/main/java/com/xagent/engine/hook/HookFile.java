package com.xagent.engine.hook;


public interface HookFile {


    public String getSourceFilePath();
    public String getPropagateFilePath();
    public String getSinkFilePath();

    public String getPropagateFileContent();

    public String getSinkFileContent();

    public String getSourceFileContent();
}
