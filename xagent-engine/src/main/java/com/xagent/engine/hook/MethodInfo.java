package com.xagent.engine.hook;

public interface MethodInfo {


    public MethodInfo setClassName(String className);

    public MethodInfo setMethodDescriptor(String methodDescriptor);

    public MethodInfo setMethodName(String methodName);

    public MethodInfo setSource(String source);

    public MethodInfo setTarget(String target);


    public String getClassName();

    public String getId();

    public String getMethodDescriptor();

    public String getMethodName();

    public String getSource();

    public String getTarget();
    int getIndex();

    void setIndex(int index);
}
