package com.xagent.engine.hook.impl;


import com.xagent.engine.hook.MethodInfo;

public class MethodInfoImpl implements MethodInfo {

    private String id;
    private String className;
    private String methodName;
    private String methodDescriptor;
    private String source;
    private int index;
    /**
     * 污染数据的流向
     * O代表当前对象自己
     * R代表返回值
     */
    private String target;


    public MethodInfo setId(String id) {
        this.id = id;

        return this;
    }

    public MethodInfoImpl(String id, String className) {
        this.id = id;
        this.className = className;
    }

    public MethodInfoImpl(String id, String className, String methodName, String methodDescriptor, String source, String target) {
        this.id = id;
        this.className = className;
        this.methodName = methodName;
        this.methodDescriptor = methodDescriptor;
        this.source = source;
        this.target = target;
    }

    public MethodInfo setClassName(String className) {
        this.className = className;

        return this;
    }

    public MethodInfo setMethodDescriptor(String methodDescriptor) {
        this.methodDescriptor = methodDescriptor;

        return this;
    }

    public MethodInfo setMethodName(String methodName) {
        this.methodName = methodName;

        return this;
    }

    public MethodInfo setSource(String source) {
        this.source = source;

        return this;
    }

    public MethodInfo setTarget(String target) {
        this.target = target;

        return this;
    }


    public String getClassName() {
        return className;
    }

    public String getId() {
        return id;
    }

    public String getMethodDescriptor() {
        return methodDescriptor;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
