package com.xagent.engine.plugins;

import com.xagent.core.Properties;
import com.xagent.engine.config.Config;
import com.xagent.engine.context.AgentContext;
import com.xagent.engine.context.inter.Context;
import com.xagent.engine.hook.HookInfo;
import com.xagent.engine.policy.PolicyBuilder;

import java.io.*;
import java.util.*;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class StartupPlugin extends Plugin {
    @Override
    public void run(Context context) throws Exception {
        Config config = context.getConfig();
        String policyPath = System.getProperty(Properties.XAGENT_POLICY_PATH);
        parsePolicyFile(policyPath, config);
    }

    public void parsePolicyFile(String policyPath, Config config) throws FileNotFoundException {
        agentLogger.info("[Agent] " + "parsing policy file.");
        /**
         * test code start
         * hook source
         * */
        InputStream sourceStream = null;
        InputStream propagateStream = null;
        InputStream sinkStream = null;
        String sourcePolicy = "source.xml";
        String propagatePolicy = "propagate.xml";
        String sinkPolicy = "sink.xml";
        HashMap<String, HashMap<String, HookInfo>> hooks = config.getPolicyMap();
        if (policyPath != null && !"".equals(policyPath) && new File(policyPath).exists() && !new File(policyPath).isDirectory()) {
            agentLogger.error("[XAGENT] " + "policy path error.");
            return;
        }
        if (policyPath != null && !policyPath.endsWith(File.pathSeparator)) {
            policyPath = policyPath + File.pathSeparator;
            sourcePolicy = policyPath + sourcePolicy;
            propagatePolicy = policyPath + propagatePolicy;
            sinkPolicy = policyPath + sinkPolicy;
        }


        if (new File(sourcePolicy).exists()) {
            sourceStream = new FileInputStream(sourcePolicy);
        } else {
            sourceStream = loadPolicyStream(AgentContext.getContext().getConfig().getAgentFilePath().getEnginePath(), sourcePolicy);
        }

        if (new File(propagatePolicy).exists()) {
            propagateStream = new FileInputStream(propagatePolicy);
        } else {
            propagateStream = loadPolicyStream(AgentContext.getContext().getConfig().getAgentFilePath().getEnginePath(), propagatePolicy);
        }

        if (new File(sinkPolicy).exists()) {
            sinkStream = new FileInputStream(sinkPolicy);
        } else {
        sinkStream = loadPolicyStream(AgentContext.getContext().getConfig().getAgentFilePath().getEnginePath(), sinkPolicy);
        }

        PolicyBuilder.parsePolicyByInputStream(sourceStream, hooks, "source");
        PolicyBuilder.parsePolicyByInputStream(propagateStream, hooks, "propagate");
        PolicyBuilder.parsePolicyByInputStream(sinkStream, hooks, "sink");

        /**
         * test code end
         * */
    }

    public InputStream loadPolicyStream(String jarPath, String resourceName) {
        try {
            JarFile jarFile = new JarFile(jarPath);
            ZipEntry entry = jarFile.getEntry(resourceName);
            if (entry != null) {
                return jarFile.getInputStream(entry);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
