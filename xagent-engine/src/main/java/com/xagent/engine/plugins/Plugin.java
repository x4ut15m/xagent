package com.xagent.engine.plugins;

import com.xagent.engine.context.AgentContext;
import com.xagent.engine.context.inter.Context;
import org.slf4j.Logger;

abstract public class Plugin {

    public boolean isRunning = false;

    public boolean isPause = false;

    public boolean isRunning() {
        return isRunning;
    }

    public boolean isPause() {
        return isPause;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public void setPause(boolean pause) {
        isPause = pause;
    }

    public void start() {
        setRunning(true);
        setPause(false);
    }


    public Logger agentLogger = AgentContext.getContext().getAgentLogger();
    public Logger securityLogger = AgentContext.getContext().getSecurityLogger();

    public void pause() {
        setPause(true);
        setRunning(false);
    }

    public String getPluginName() {
        return this.getClass().getSimpleName();
    }

    public abstract void run(Context context) throws Exception;

    public void start(Context context) throws Exception {
        start();
        if (!Plugin.this.getPluginName().equals("StartupPlugin")) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Plugin.this.run(context);
                    } catch (Exception e) {
                        //
                    }
                }
            });
            thread.start();
        } else Plugin.this.run(context);

    }


}
