package com.xagent.engine.plugins;

import com.xagent.engine.context.inter.Context;
import com.xagent.engine.context.inter.InstWrapper;
import com.xagent.engine.hook.transformer.DefaultTransformer;

import java.util.Base64;

public class HookPlugin extends Plugin {

    @Override
    public void run(Context context) throws Exception {
        InstWrapper instrumentation = context.getInstWrapper();
        instrumentation.addTransformer(new DefaultTransformer());
        instrumentation.retransformClasses(Runtime.class);
        instrumentation.retransformClasses(Base64.class);
        instrumentation.retransformClasses(StringBuilder.class);
    }
}
