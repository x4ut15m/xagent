package com.xagent.engine.plugins;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.FileAppender;
import com.xagent.engine.context.inter.Context;
import org.slf4j.LoggerFactory;

public class LoggerPlugin extends Plugin {
    @Override
    public void run(Context agentContext) throws Exception {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(context);
        context.reset(); // 重置现有配置


// 配置 Agent Appender
        FileAppender agentAppender = new FileAppender();
        agentAppender.setContext(context);
        agentAppender.setName("AGENT-APPENDER");
        agentAppender.setFile("logs/agent.log");

        PatternLayoutEncoder agentEncoder = new PatternLayoutEncoder();
        agentEncoder.setContext(context);
        agentEncoder.setPattern("%date %level [%thread] %logger{10} [%file:%line] %msg%n");
        agentEncoder.start();

        agentAppender.setEncoder(agentEncoder);
        agentAppender.start();

// 配置 Agent Logger
        Logger agentLogger = context.getLogger("agentLogger");
        agentLogger.addAppender(agentAppender);
        agentLogger.setLevel(Level.DEBUG);
        agentLogger.setAdditive(false); // 不向上级日志传递

        // 配置 Security Appender
        FileAppender securityAppender = new FileAppender();
        securityAppender.setContext(context);
        securityAppender.setName("SECURITY-APPENDER");
        securityAppender.setFile("logs/security.log");

        PatternLayoutEncoder securityEncoder = new PatternLayoutEncoder();
        securityEncoder.setContext(context);
        securityEncoder.setPattern("%date %level [%thread] %logger{10} [%file:%line] %msg%n");
        securityEncoder.start();

        securityAppender.setEncoder(securityEncoder);
        securityAppender.start();

        // 配置 Security Logger
        Logger securityLogger = context.getLogger("securityLogger");
        securityLogger.addAppender(securityAppender);
        securityLogger.setLevel(Level.DEBUG);
        securityLogger.setAdditive(false); // 不向上级日志传递

        // 设置 Root Logger 的级别
        Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);
        rootLogger.setLevel(Level.INFO);

    }
}
