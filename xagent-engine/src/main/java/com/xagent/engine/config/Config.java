package com.xagent.engine.config;

import com.xagent.core.FilePath;
import com.xagent.engine.hook.HookInfo;

import java.util.HashMap;

public interface Config {

    HashMap<String, HashMap<String, HookInfo>> getPolicyMap();

    /**
     * @return 引擎jar所在的目录
     */
    FilePath getAgentFilePath();
}
