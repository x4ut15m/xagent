package com.xagent.engine.config;

import com.xagent.core.FilePath;
import com.xagent.core.Properties;
import com.xagent.engine.ReflectEntry;
import com.xagent.engine.hook.HookInfo;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;

public class ConfigImpl implements Config {
    private HashMap<String, HashMap<String, HookInfo>> policyMap;

    private FilePath agentFilePath;

    public ConfigImpl(FilePath filePath) {
        policyMap = new HashMap<>();
        policyMap.put("source", new HashMap<String, HookInfo>());
        policyMap.put("propagate", new HashMap<String, HookInfo>());
        policyMap.put("sink", new HashMap<String, HookInfo>());
        agentFilePath = filePath;

    }


    /**
     * @return 如果设置了agentPath则返回设置的agentPath, 否则返回engine.jar的path.
     */
    public FilePath getAgentFilePath() {
        if (agentFilePath != null) return agentFilePath;

        return null;
    }

    @Override
    public HashMap<String, HashMap<String, HookInfo>> getPolicyMap() {
        return policyMap;
    }
}
